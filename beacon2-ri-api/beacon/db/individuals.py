import logging
from typing import Dict, List, Optional
from beacon.db.filters import apply_alphanumeric_filter, apply_filters
from beacon.db.utils import query_id, query_ids, get_count, get_documents, get_cross_query
from beacon.db import client, Patient_dictConfig
from beacon.request.model import AlphanumericFilter, Operator, RequestParams
from beacon.db.schemas import DefaultSchemas
from beacon.request.model import RequestParams


from beacon.db.filtering_terms import return_filtering_terms

import json
from urllib.request import urlopen



LOG = logging.getLogger(__name__)


# Individual Data
print(Patient_dictConfig)

def format_query(listPatients):
    list_format = []
    for patientDict in listPatients:
        dictId = {'id':patientDict["id"]}
        if "sexLabel" in patientDict and "sexId" in patientDict:
            dictId["sex"] = {
                                "id":patientDict["sexId"],
                                "label":patientDict["sexLabel"]
                            }
        else:
            # Required variable
            dictId["sex"] = {
                    "id":'NCIT:C53269',
                    "label":'No Information Available'
                }
        if 'diseases' in patientDict:
            listDiseases = []
            for disease in patientDict['diseases']:
                dictDisease ={'diseaseCode': {
                                "id":disease["diseaseId"],
                                "label":disease["diseaseLabel"]
                            }
                        }
                if "diseaseAge" in patientDict:
                    dictDisease["age"] = "P" + patientDict["diseaseAge"] + "Y"
                listDiseases.append(dictDisease)
            dictId['disease'] = listDiseases
        if "ethnicityLabel" in patientDict and "ethnicityId" in patientDict:
            dictId["ethnicity"] = {
                                "id":patientDict["ethnicityId"],
                                "label":patientDict["ethnicityLabel"]
                            }
        if "treatments" in patientDict:
            list_treatments = []
            if len(patientDict['treatments']['treatmentId']):
                for treatmentIndex in range(len(patientDict['treatments']['treatmentId'])):
                    treatmentDict = {'treatmentCode': {
                                "id":patientDict["treatments"]['treatmentId'][treatmentIndex],
                                "label":patientDict["treatments"]["treatmentLabel"][treatmentIndex]
                            }
                        }
                    list_treatments.append(treatmentDict)
                dictId["treatments"] = list_treatments
        list_format.append(dictId)
    return list_format


def checkPatientData(data, filter):
    # Check if the request parameters are true
    if not data[0]["value"]==filter:
            return False
    return True

def retrieveDiseaseData(samplePatientId, configVariables):
    listDiseases = []
    # Retrieve all samples from patient
    urlData=f"{client['API_URL']}/studies/{client['studyId']}/patients/{samplePatientId}/samples"
    response = urlopen(urlData)
    dataJson = json.loads(response.read())
    for sampleEntry in dataJson:
        sampleId = sampleEntry['sampleId']
        dictDisease = {}
        for value in configVariables['diseaseId']:
            # Cancer ontology type for each sample
            urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{sampleId}/clinical-data?attributeId={value}"
            response = urlopen(urlData)
            dataJson = json.loads(response.read())
            if len(dataJson)==0:
                dictDisease['diseaseId'] = 'NCIT:C53269'
            else:
                dictDisease['diseaseId'] = dataJson[0]["value"]
        for value in configVariables['diseaseLabel']:
            # Cancer label for each sample
            urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{sampleId}/clinical-data?attributeId={value}"
            response = urlopen(urlData)
            dataJson = json.loads(response.read())
            if len(dataJson)==0:
                dictDisease['diseaseLabel'] = 'No Information Available'
            else:
                dictDisease['diseaseLabel'] = dataJson[0]["value"]
            listDiseases.append(dictDisease)

    return listDiseases

def retrievePatientData(samplePatientId, configVariables):
    dictSamplePatientId = {'id': samplePatientId}
    diseaseNotRetrieved = True
    treatmentsNotRetrived = True
    for configVariable in configVariables:					# For each variable in the config.ini file
        for value in configVariables[configVariable]:
            if 'diseaseLabel' in configVariable or 'diseaseId' in configVariable and diseaseNotRetrieved:
                listDiseases = retrieveDiseaseData(samplePatientId, configVariables)
                diseaseNotRetrieved = False
                dictSamplePatientId['diseases'] = listDiseases
            else:
                # API search
                urlData=f"{client['API_URL']}/studies/{client['studyId']}/patients/{samplePatientId}/clinical-data?attributeId={value}"
                response = urlopen(urlData)
                dataJson = json.loads(response.read())
                if not dataJson:	# If variable do not have any values, remove it
                    continue
                if 'treatment' in configVariable:
                    if treatmentsNotRetrived:
                        dictSamplePatientId['treatments'] = {'treatmentId':[], 'treatmentLabel':[]}
                        treatmentsNotRetrived = False
                    dictSamplePatientId['treatments'][configVariable] = dictSamplePatientId['treatments'][configVariable] +  [dataJson[0]["value"]]
                else:
                    dictSamplePatientId[configVariable] = dataJson[0]["value"]
    return dictSamplePatientId

def checkDiseaseData(samplePatientId, configVariables, filter):
    urlData=f"{client['API_URL']}/studies/{client['studyId']}/patients/{samplePatientId}/samples"
    response = urlopen(urlData)
    dataJson = json.loads(response.read())
    for sampleEntry in dataJson:
        sampleId = sampleEntry['sampleId']
        # Cancer ontology type for each sample
        # Change to Id
        for value in configVariables['diseaseLabel']:
            urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{sampleId}/clinical-data?attributeId={value}"
            response = urlopen(urlData)
            dataJson = json.loads(response.read())
            if checkPatientData(dataJson, filter):
                return True
    return False


# Extract Clinical Data from Patients and Samples
def query_API_filters(query, skip, limit, configVariables):
    counter_skip = skip
    patientORsampleId = 'patientId'
    counter_PatientData=0

	# List of Ids of the patients and samples
    samplePatientIdsJson = f"{client['API_URL']}/studies/{client['studyId']}/patients"
    response = urlopen(samplePatientIdsJson)
    samplePatientIdsJson = json.loads(response.read())
    # Extract Sample or Patient data
    listPatients = [] # List of all patient or sample data
    for samplePatientIdvar in samplePatientIdsJson:					# For each patientId
        samplePatientId = samplePatientIdvar[patientORsampleId]
        is_patient= False
        for filter in query:
            is_filter = False
            for configVariable in configVariables:					# For each variable in the config.ini file
                if 'Label' in configVariable:
                        continue
                if "diseaseId" in configVariable:
                    isPatientValue = checkDiseaseData(samplePatientId, configVariables, filter)
                    if not isPatientValue:
                            continue
                    is_filter = True
                    is_patient = True
                    break        
                for value in configVariables[configVariable]:
                    # API search
                    urlData=f"{client['API_URL']}/studies/{client['studyId']}/patients/{samplePatientId}/clinical-data?attributeId={value}"
                    response = urlopen(urlData)
                    dataJson = json.loads(response.read())
                    if not dataJson:	# If variable do not have any values, remove it
                        continue
                    if query:
                        isPatientValue = checkPatientData(dataJson, filter)
                        if not isPatientValue:
                            continue
                    is_filter = True
                    is_patient = True
                    break
            if not is_filter:
                is_patient = False
                break
        if not is_patient:
            continue
        counter_PatientData +=1
        if counter_skip<=0 and counter_PatientData<=limit:
            patientData = retrievePatientData(samplePatientId, configVariables)
            listPatients.append(patientData)
        else:
            counter_skip -= 1
    return listPatients, counter_PatientData

# Extract Clinical Data from Patients and Samples
def query_API_all( patientId, skip, limit, configVariables):
    counter_skip = skip
    patientORsampleId = 'patientId'
    counter_PatientData=0

	# List of Ids of the patients and samples
    if patientId:
        samplePatientIdsJson = [patientId]
    else:
        samplePatientIdsJson = f"{client['API_URL']}/studies/{client['studyId']}/patients"
        response = urlopen(samplePatientIdsJson)
        samplePatientIdsJson = json.loads(response.read())
    # Extract Sample or Patient data
    listPatients = [] # List of all patient or sample data
    for samplePatientIdvar in samplePatientIdsJson:					# For each patientId
        if type(samplePatientIdvar) is dict:
            samplePatientId = samplePatientIdvar[patientORsampleId]
        else:
            samplePatientId = samplePatientIdvar
        for configVariable in configVariables:
            for value in configVariables[configVariable]:					# For each variable in the config.ini file
                # API search
                urlData=f"{client['API_URL']}/studies/{client['studyId']}/patients/{samplePatientId}/clinical-data?attributeId={value}"
                response = urlopen(urlData)
                dataJson = json.loads(response.read())
                if not dataJson:	# If variable do not have any values, remove it
                    continue
                isVariable = True
            if isVariable:
                break
        counter_PatientData +=1
        if counter_skip<=0 and counter_PatientData<=limit:
            patientData = retrievePatientData(samplePatientId, configVariables)
            listPatients.append(patientData)
        else:
            counter_skip -= 1
    return listPatients, counter_PatientData

def apply_request_parameters(query: Dict[str, List[dict]], qparams: RequestParams):
    LOG.debug("Request parameters len = {}".format(len(qparams.query.request_parameters)))
    for k, v in qparams.query.request_parameters.items():
        query["$text"] = {}
        if ',' in v:
            v_list = v.split(',')
            v_string=''
            for val in v_list:
                v_string += f'"{val}"'
            query["$text"]["$search"]=v_string
        else:
            query["$text"]["$search"]=v
    return query

def get_individuals(entry_id: Optional[str], qparams: RequestParams):

    schema = DefaultSchemas.INDIVIDUALS

    if qparams.query.filters:
        if type(qparams.query.filters[0]) is dict:
            qparams.query.filters = [i['id'] for i in qparams.query.filters]
            
        # Search for the data in cBioPortal
        indData_cBioPortal, lenData = query_API_filters(qparams.query.filters,
                                    qparams.query.pagination.skip,
                                    qparams.query.pagination.limit,
                                    Patient_dictConfig)
        if lenData == 0:
            return schema, lenData, []
    else:
        indData_cBioPortal, lenData = query_API_all(None, qparams.query.pagination.skip,
                            qparams.query.pagination.limit,
                            Patient_dictConfig)
    docs =format_query(indData_cBioPortal)

    return schema, lenData, docs


def get_individual_with_id(entry_id: Optional[str], qparams: RequestParams):
    indData_cBioPortal, lenData = query_API_all(entry_id, qparams.query.pagination.skip,
                            qparams.query.pagination.limit,
                            Patient_dictConfig)
    docs =format_query(indData_cBioPortal)
    schema = DefaultSchemas.INDIVIDUALS

    return schema, lenData, docs


def get_variants_of_individual(entry_id: Optional[str], qparams: RequestParams):
    collection = 'individuals'
    query = {"$and": [{"id": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    count = get_count(client.beacon.individuals, query)
    individual_ids = client.beacon.individuals \
        .find_one(query, {"id": 1, "_id": 0})
    LOG.debug(individual_ids)
    individual_ids=get_cross_query(individual_ids,'id','caseLevelData.biosampleId')
    LOG.debug(individual_ids)
    query = apply_filters(individual_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.GENOMICVARIATIONS
    count = get_count(client.beacon.genomicVariations, query)
    docs = get_documents(
        client.beacon.genomicVariations,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs


def get_biosamples_of_individual(entry_id: Optional[str], qparams: RequestParams):
    collection = 'individuals'
    query = {"individualId": entry_id}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    schema = DefaultSchemas.BIOSAMPLES
    count = get_count(client.beacon.biosamples, query)
    docs = get_documents(
        client.beacon.biosamples,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs

def get_filtering_terms_of_individual(entry_id: Optional[str], qparams: RequestParams):
    schema, len_filters, listFilters = return_filtering_terms('individuals', Patient_dictConfig)
    return schema, len_filters, listFilters