from typing import Optional
from beacon.db import client, Patient_dictConfig, Sample_dictConfig

from beacon.db.filters import apply_filters
from beacon.db.utils import query_id, get_documents, get_count
from beacon.request.model import RequestParams

from beacon.db.schemas import DefaultSchemas

import json
from urllib.request import urlopen


def get_filtering_terms(entry_id: Optional[str], qparams: RequestParams):
    schema = None
    schemaInd, indCount, indDocs = return_filtering_terms('individuals', Patient_dictConfig)
    schemaInd, bioCount, bioDocs = return_filtering_terms('biosamples', Sample_dictConfig)

    return schema, indCount + bioCount, indDocs + bioDocs



def get_filtering_term_with_id(entry_id: Optional[str], qparams: RequestParams):
    query = apply_filters({}, qparams.query.filters)
    query = query_id(query, entry_id)
    schema = None
    count = get_count(client.beacon.filtering_terms, query)
    docs = get_documents(
        client.beacon.filtering_terms,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs

def retrieveLabel(patientId, Patient_dictConfig,
                   propertiesPatient, indexValue,
                   modelcBioPortal):
    # Figure out which is the label of the filter. Remove Id and add label
    # search url
    # retrieve label
    if not propertiesPatient.endswith('Id'):
        return False
    propertyLabel = propertiesPatient[:-2] + 'Label'
    varibalecBioPortal = Patient_dictConfig[propertyLabel][indexValue]
    getFilterUrl = f"{client['API_URL']}/studies/{client['studyId']}/{modelcBioPortal[0]}/{patientId}/clinical-data?attributeId={varibalecBioPortal}"
    print(getFilterUrl)
    response = urlopen(getFilterUrl)
    getFilterJson = json.loads(response.read())
    return getFilterJson[0]['value']

def return_API_terms(dictConfig,propertiesPatient, model,
                              modelcBioPortal, setFilterCheck,
                              listFilters):

    indexValue = 0
    for value in dictConfig[propertiesPatient]:
        getFilterUrl = f"{client['API_URL']}/studies/{client['studyId']}/clinical-data?clinicalDataType={modelcBioPortal[1]}&attributeId={value}"
        response = urlopen(getFilterUrl)
        getFilterJson = json.loads(response.read())
        for patientValue in getFilterJson:
            if patientValue['value'] in setFilterCheck:
                continue
            setFilterCheck.add(patientValue['value'])

            patientId = patientValue['patientId']
            if 'sampleId' in patientValue:
                patientId = patientValue['sampleId']

            labelFilter = retrieveLabel(patientId, dictConfig,
                                         propertiesPatient, indexValue,
                                         modelcBioPortal)
            if labelFilter is False:
                continue
            dict_filter = {"id":patientValue['value'],"label":labelFilter,"scopes":[model],"type":"ontology"}
            listFilters.append(dict_filter)
        indexValue += 1
    return listFilters, setFilterCheck

def return_filtering_terms(model, dictConfig):
    schema = DefaultSchemas.FILTERINGTERMS
    listFilters = []
    setFilterCheck = set()
    if model=='individuals':
        modelcBioPortal = ['patients', 'PATIENT']
    else:
        modelcBioPortal = ['samples', 'SAMPLE']
    for propertiesPatient in dictConfig:
        if 'Label' in propertiesPatient:
            continue
        if 'diseaseId' in propertiesPatient:
            list_diseases_filter, setFilterCheck = return_API_terms(dictConfig,propertiesPatient, model,
                              ['samples', 'SAMPLE'], setFilterCheck,
                              listFilters)
            listFilters = listFilters + list_diseases_filter
        else:
            list_general_filter, setFilterCheck = return_API_terms(dictConfig,propertiesPatient, model,
                              modelcBioPortal, setFilterCheck,
                              listFilters)
            listFilters = listFilters + list_general_filter
        
    return schema, len(listFilters), listFilters
