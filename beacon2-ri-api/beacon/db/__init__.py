from beacon import conf
import os
import sys
import configparser

API_URL = os.getenv('cBioPortal_URL', default="https://www.cbioportal.org/api")
# If statement in case the person give a 'localhost:XXXX' URL
if API_URL.startswith('localhost:'):
    API_URL = 'http://' + API_URL

client = {'API_URL': API_URL,
          'studyId': os.getenv('study_name', default="acc_tcga")}


# Read Config File
## This file will show which variables are related to the Beacon properties
# Configuration file as first argument
configPath=sys.argv[1]

# Read config.ini
def readIni(configPath):
	config = configparser.ConfigParser()
	config.optionxform=str

	config.read(configPath)
	dictConfig = {}
	for section in config.sections():
		dictConfig[section] = {}
		for key in config[section]:
			value = config[section][key]
			# Avoid empty variables
			if not value:
				continue
			# Write config values to the dict
			# If multiple column for a beacon property
			# Separate by comma and remove blank spaces
			if ',' in value:
				value = [x.strip() for x in value.split(',')]
				dictConfig[section][key] = value
			else:
				dictConfig[section][key] = [value]
	return dictConfig

dictConfig = readIni(configPath)
Patient_dictConfig = dictConfig['Patient Data']
Sample_dictConfig = dictConfig['Sample Data']