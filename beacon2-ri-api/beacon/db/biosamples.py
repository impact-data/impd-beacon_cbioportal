import logging
from typing import Dict, List, Optional
from beacon.db.filters import apply_alphanumeric_filter, apply_filters
from beacon.db.utils import query_id, query_ids, get_count, get_documents, get_cross_query
from beacon.db import client, Sample_dictConfig
from beacon.request.model import AlphanumericFilter, Operator, RequestParams
from beacon.db.schemas import DefaultSchemas
from beacon.request.model import RequestParams

from beacon.db.filtering_terms import return_filtering_terms

import json
from urllib.request import urlopen


LOG = logging.getLogger(__name__)

# Biosample Data
print(Sample_dictConfig)

def format_query(listSamples):
    list_format = []
    for sampleDict in listSamples:
        dictId = {'id':sampleDict["id"]}
        dictId["IndividualId"] = sampleDict["patientId"]
        if "sampleOriginDetailId" in sampleDict:
            dictId["sampleOriginDetail"] ={
                "label":sampleDict["sampleOriginDetailLabel"],
                "id": sampleDict["sampleOriginDetailId"]
            }
        if "TumourProgressionId" in sampleDict:
            dictId["TumourProgression"] ={
                "label":sampleDict["TumourProgressionLabel"],
                "id": sampleDict["TumourProgressionId"]
            }
        if "pathologicalStageId" in sampleDict:
            dictId["pathologicalStage"] ={
                "label":sampleDict["pathologicalStageLabel"],
                "id": sampleDict["pathologicalStageId"]
            }
        if "histologicalDiagnosisId" in sampleDict:
            dictId["histologicalDiagnosis"] ={
                "label":sampleDict["histologicalDiagnosisLabel"],
                "id": sampleDict["histologicalDiagnosisId"]
            }
        if "pathologicalTnmFinding" in sampleDict:
            list_PTnmF = []
            for PTnmFIndex in range(len(sampleDict['pathologicalTnmFinding']['pathologicalTnmFindingId'])):
                PTnmFDict = {
                                    "id":sampleDict["pathologicalTnmFinding"]['pathologicalTnmFindingId'][PTnmFIndex],
                                    "label":sampleDict["pathologicalTnmFinding"]["pathologicalTnmFindingLabel"][PTnmFIndex]
                                }
                list_PTnmF.append(PTnmFDict)
            dictId["pathologicalTnmFinding"] = list_PTnmF
        # Add unused required properties
        dictId['biosampleStatus'] = {'id':'NCIT:C53269', 'label':'No Information Available'}
        dictId['sampleOriginType'] = {'id':'NCIT:C53269', 'label':'No Information Available'}
        # Add biosamples variables
        list_format.append(dictId)
    return list_format


def checkSampleData(data, filter):
    # Check if the request parameters are true
    if not data[0]["value"]==filter:
            return False
    return True

def retrieveSampleData(samplePatientId, configVariables):
    dictSamplePatientId = {'id': samplePatientId}
    isPatientId = False
    pathologicalTnmFindingNotReceived = True
    for configVariable in configVariables:					# For each variable in the config.ini file
        for value in configVariables[configVariable]:
            # API search
            urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{samplePatientId}/clinical-data?attributeId={value}"
            response = urlopen(urlData)
            dataJson = json.loads(response.read())
            if not dataJson:	# If variable do not have any values, remove it
                continue
            if not isPatientId:
                dictSamplePatientId["patientId"] = dataJson[0]["patientId"]
                isPatientId = True
            if 'pathologicalTnmFinding' in configVariable:
                if pathologicalTnmFindingNotReceived:
                    dictSamplePatientId['pathologicalTnmFinding'] = {'pathologicalTnmFindingId':[], 'pathologicalTnmFindingLabel':[]}
                    pathologicalTnmFindingNotReceived = False
                dictSamplePatientId['pathologicalTnmFinding'][configVariable] = dictSamplePatientId['pathologicalTnmFinding'][configVariable] +  [dataJson[0]["value"]]
            else:
                dictSamplePatientId[configVariable] = dataJson[0]["value"]

    
    return dictSamplePatientId


# Extract Clinical Data from Patients and Samples
def query_API_filters(query, skip, limit, configVariables):
    counter_skip = skip
    patientORsampleId = 'sampleId'
    counter_SampleData=0

	# List of Ids of the patients and samples
    samplePatientIdsJson = f"{client['API_URL']}/studies/{client['studyId']}/samples"
    response = urlopen(samplePatientIdsJson)
    samplePatientIdsJson = json.loads(response.read())
    # Extract Sample or Patient data
    listSamples = [] # List of all patient or sample data
    for samplePatientIdvar in samplePatientIdsJson:					# For each patientId
        samplePatientId = samplePatientIdvar[patientORsampleId]
        is_sample = False
        for filter in query:
            is_filter = False
            for configVariable in configVariables:
                if 'Label' in configVariable:
                        continue				# For each variable in the config.ini file
                for value in configVariables[configVariable]:
                    # API search
                    urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{samplePatientId}/clinical-data?attributeId={value}"
                    response = urlopen(urlData)
                    dataJson = json.loads(response.read())
                    if not dataJson:	# If variable do not have any values, remove it
                        continue
                    if query:
                        isSampleValue = checkSampleData(dataJson, filter)
                        if not isSampleValue:
                            continue
                    is_filter = True
                    is_sample = True
                    break
            if not is_filter:
                is_sample = False
                break
        if not is_sample:
            continue
        counter_SampleData +=1
        if counter_skip<=0 and counter_SampleData<=limit:
            sampleData = retrieveSampleData(samplePatientId, configVariables)
            listSamples.append(sampleData)
        else:
            counter_skip -= 1
    return listSamples, counter_SampleData


# Extract Clinical Data from Patients and Samples
def query_API_all( sampleId, skip, limit, configVariables):
    counter_skip = skip
    patientORsampleId = 'sampleId'
    counter_SampleData=0

	# List of Ids of the patients and samples
    if sampleId:
        samplePatientIdsJson = [sampleId]
    else:
        samplePatientIdsJson = f"{client['API_URL']}/studies/{client['studyId']}/samples"
        response = urlopen(samplePatientIdsJson)
        samplePatientIdsJson = json.loads(response.read())
    # Extract Sample or Patient data
    listSamples = [] # List of all patient or sample data
    for samplePatientIdvar in samplePatientIdsJson:					# For each patientId
        if type(samplePatientIdvar) is dict:
            samplePatientId = samplePatientIdvar[patientORsampleId]
        else:
            samplePatientId = samplePatientIdvar
        for configVariable in configVariables:					# For each variable in the config.ini file
            for value in configVariables[configVariable]:
                # API search
                urlData=f"{client['API_URL']}/studies/{client['studyId']}/samples/{samplePatientId}/clinical-data?attributeId={value}"
                response = urlopen(urlData)
                dataJson = json.loads(response.read())
                if not dataJson:	# If variable do not have any values, remove it
                    isVariable = False
                    continue
                isVariable = True
            if isVariable:
                break
        counter_SampleData +=1
        if counter_skip<=0 and counter_SampleData<=limit:
            sampleData = retrieveSampleData(samplePatientId, configVariables)
            listSamples.append(sampleData)
        else:
            counter_skip -= 1
    return listSamples, counter_SampleData


def apply_request_parameters(query: Dict[str, List[dict]], qparams: RequestParams):
    LOG.debug("Request parameters len = {}".format(len(qparams.query.request_parameters)))
    for k, v in qparams.query.request_parameters.items():
        query["$text"] = {}
        if ',' in v:
            v_list = v.split(',')
            v_string=''
            for val in v_list:
                v_string += f'"{val}"'
            query["$text"]["$search"]=v_string
        else:
            query["$text"]["$search"]=v
    return query


def get_biosamples(entry_id: Optional[str], qparams: RequestParams):
    schema = DefaultSchemas.BIOSAMPLES

    if qparams.query.filters:
        if type(qparams.query.filters[0]) is dict:
            qparams.query.filters = [i['id'] for i in qparams.query.filters]

        # Search for the data in cBioPortal
        biosampleData_cBioPortal, lenData = query_API_filters(qparams.query.filters,
                                    qparams.query.pagination.skip,
                                    qparams.query.pagination.limit,
                                    Sample_dictConfig)
        if lenData == 0:
            return schema, lenData, []
    else:
        biosampleData_cBioPortal, lenData = query_API_all(None, qparams.query.pagination.skip,
                            qparams.query.pagination.limit,
                            Sample_dictConfig)
    docs =format_query(biosampleData_cBioPortal)
    return schema, lenData, docs

def get_biosample_with_id(entry_id: Optional[str], qparams: RequestParams):
    biosampleData_cBioPortal, lenData = query_API_all(entry_id, qparams.query.pagination.skip,
                            qparams.query.pagination.limit,
                            Sample_dictConfig)
    docs =format_query(biosampleData_cBioPortal)
    schema = DefaultSchemas.BIOSAMPLES

    return schema, lenData, docs

def get_variants_of_biosample(entry_id: Optional[str], qparams: RequestParams):
    collection = 'biosamples'
    query = {"$and": [{"id": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    count = get_count(client.beacon.biosamples, query)
    biosamples_ids = client.beacon.biosamples \
        .find_one(query, {"id": 1, "_id": 0})
    LOG.debug(biosamples_ids)
    biosamples_ids=get_cross_query(biosamples_ids,'id','caseLevelData.biosampleId')
    LOG.debug(biosamples_ids)
    query = apply_filters(biosamples_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.GENOMICVARIATIONS
    count = get_count(client.beacon.genomicVariations, query)
    docs = get_documents(
        client.beacon.genomicVariations,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs

def get_filtering_terms_of_biosample(entry_id: Optional[str], qparams: RequestParams):
    schema, len_filters, listFilters = return_filtering_terms('biosamples', Sample_dictConfig)

    return schema, len_filters, listFilters