import logging
from typing import Dict, List, Optional
from beacon.db.filters import apply_alphanumeric_filter, apply_filters
from beacon.db.schemas import DefaultSchemas
from beacon.db.utils import query_id, query_ids, get_count, get_documents, get_cross_query, get_cross_query_variants
from beacon.request.model import AlphanumericFilter, Operator, RequestParams
from beacon.db import client
import json
from bson import json_util
from urllib.request import urlopen

LOG = logging.getLogger(__name__)

# There is not: Aminoacid Change, Genomic allele short form,
VARIANTS_PROPERTY_MAP = {
    "assemblyId": "ncbiBuild",
    "referenceName": "chr", 
    "start": "startPosition", 
    "end": "endPosition", 
    "referenceBases": "referenceAllele", 
    "alternateBases": "variantAllele",
    "variantType": "variantType",
    "variantMinLength": None,
    "variantMaxLength": None,
    "mateName": "refseqMrnaId", # We need to check if its true. I don't know what is mateName
    "gene": "entrezGeneId",
}

def generate_position_filter_start(key: str, value: List[int]) -> List[AlphanumericFilter]:
    LOG.debug("len value = {}".format(len(value)))
    filters = []
    if len(value) == 1:
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[0],
            operator=Operator.GREATER_EQUAL # I don't know why this is not equal
        ))
    elif len(value) == 2:
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[0],
            operator=Operator.GREATER_EQUAL
        ))
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[1],
            operator=Operator.LESS_EQUAL
        ))
    return filters


def generate_position_filter_end(key: str, value: List[int]) -> List[AlphanumericFilter]:
    LOG.debug("len value = {}".format(len(value)))
    filters = []
    if len(value) == 1:
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[0],
            operator=Operator.LESS_EQUAL
        ))
    elif len(value) == 2:
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[0],
            operator=Operator.GREATER_EQUAL
        ))
        filters.append(AlphanumericFilter(
            id=VARIANTS_PROPERTY_MAP[key],
            value=value[1],
            operator=Operator.LESS_EQUAL
        ))
    return filters


def apply_request_parameters(query: Dict[str, List[dict]], qparams: RequestParams):
    collection = 'g_variants'
    LOG.debug("Request parameters len = {}".format(len(qparams.query.request_parameters)))
    if len(qparams.query.request_parameters) > 0 and "$and" not in query:
        query["$and"] = []
    for k, v in qparams.query.request_parameters.items():
        if k == "start":
            if isinstance(v, str):
                v = v.split(',')
            filters = generate_position_filter_start(k, v)
            for filter in filters:
                query["$and"].append(apply_alphanumeric_filter({}, filter, collection))
        elif k == "end":
            if isinstance(v, str):
                v = v.split(',')
            filters = generate_position_filter_end(k, v)
            for filter in filters:
                query["$and"].append(apply_alphanumeric_filter({}, filter, collection))
        elif k == "variantMinLength" or k == "variantMaxLength" or k == "mateName":
            continue
        else:
            query["$and"].append(apply_alphanumeric_filter({}, AlphanumericFilter(
                id=VARIANTS_PROPERTY_MAP[k],
                value=v
            ), collection))
    return query

def check_genomicVariant(molecularData, query):
    # Check if the request parameters are true
    for filterParam in query['$and']:
        for key, values in filterParam.items():
            if key=='chr':
                values[1] = str(values[1])
            if not eval(f'molecularData[key] {values[0]} values[1] '):
                return False
    return True

def query_API(query, skip, limit):
    counter_skip = skip

    listMolecularProfiles = f"{client['API_URL']}/studies/{client['studyId']}/molecular-profiles"
    LOG.debug("API query path {}".format(f"{client['API_URL']}/studies/{client['studyId']}"))
    listGenomicVarData = []
    counter_GenomicData=0
    response = urlopen(listMolecularProfiles)
    listMolecularProfiles = json.loads(response.read())
	# For now, we are only interested in the Mutation Data
	# Take the object with MUTATION_EXTENDED as Molecular Alteration Type
    for molecularProfiles in listMolecularProfiles:
        if molecularProfiles['molecularAlterationType'] != 'MUTATION_EXTENDED':
            continue
        molecularProfileId = molecularProfiles['molecularProfileId']
    # API to find the sample list related with mutation data
    listSampleID = f"{client['API_URL']}/studies/{client['studyId']}/sample-lists"
    response = urlopen(listSampleID)
    listSampleID = json.loads(response.read())
    # Take the sampleList Id for the mutation data -> Category = all_cases_with_mutation_data
    for sampleID in listSampleID:
        if sampleID['category'] != 'all_cases_with_mutation_data':
            continue
        sampleIdMutation=sampleID["sampleListId"]
    # After having the Molecular Profile Id and sampleList Id we can query for genomics variants data
    listMolecularData = f"{client['API_URL']}/molecular-profiles/{molecularProfileId}/mutations?sampleListId={sampleIdMutation}"
    response = urlopen(listMolecularData)
    listMolecularData = json.loads(response.read())
    for molecularData in listMolecularData:		# For all the entries having a mutation
        if query:            
            # Filter entries with the requested parameters
            isGenomicVariant = check_genomicVariant(molecularData, query)
            if not isGenomicVariant:
                continue
        counter_GenomicData +=1
        if counter_skip<=0 and len(listGenomicVarData)<limit:
            listGenomicVarData.append(molecularData)
        else:
            counter_skip -= 1    
    #     if counter_skip<=0:
    #         listGenomicVarData.append(molecularData)
    #         if len(listGenomicVarData) == limit:
    #             return listGenomicVarData
    #     else:
    #         counter_skip -= 1
    # return listGenomicVarData
    return listGenomicVarData, counter_GenomicData

def calculateHGVSId(startPosition, endPosition, chr, referenceAllele, variantAllele, ncbiBuild, variantType):
    dictRefSeqChr = {"GRCh38":{
        '1':'NC_000001.11',
        '2':'NC_000002.12',
        '3':'NC_000003.12',
        '4':'NC_000004.12',
        '5':'NC_000005.10',
        '6':'NC_000006.12',
        '7':'NC_000007.14',
        '8':'NC_000008.11',
        '9':'NC_000009.12',
        '10':'NC_000010.11',
        '11':'NC_000011.10',
        '12':'NC_000012.12',
        '13':'NC_000013.11',
        '14':'NC_000014.9',
        '15':'NC_000015.10',
        '16':'NC_000016.10',
        '17':'NC_000017.11',
        '18':'NC_000018.10',
        '19':'NC_000019.10',
        '20':'NC_000020.11',
        '21':'NC_000021.9',
        '22':'NC_000022.11',
        'X':'NC_000023.11',
        'Y':'NC_000024.10',},
        "GRCh37":{
        '1':'NC_000001.10',
        '2':'NC_000002.11',
        '3':'NC_000003.11',
        '4':'NC_000004.11',
        '5':'NC_000005.9',
        '6':'NC_000006.11',
        '7':'NC_000007.13',
        '8':'NC_000008.10',
        '9':'NC_000009.11',
        '10':'NC_000010.10',
        '11':'NC_000011.9',
        '12':'NC_000012.11',
        '13':'NC_000013.10',
        '14':'NC_000014.8',
        '15':'NC_000015.9',
        '16':'NC_000016.9',
        '17':'NC_000017.10',
        '18':'NC_000018.9',
        '19':'NC_000019.9',
        '20':'NC_000020.10',
        '21':'NC_000021.8',
        '22':'NC_000022.10',
        'X':'NC_000023.10',
        'Y':'NC_000024.9'}}
    if len(referenceAllele) == 1 and len(variantAllele) == 1:
        hgvs = f"{dictRefSeqChr[ncbiBuild][chr]}:g.{startPosition}{referenceAllele}>{variantAllele}"
    elif variantType == "DEL":
        hgvs = f"{dictRefSeqChr[ncbiBuild][chr]}:g.{startPosition}_{endPosition}del"
    elif variantType == "INS":
        hgvs = f"{dictRefSeqChr[ncbiBuild][chr]}:g.{startPosition}_{endPosition}ins{variantAllele}"
    return hgvs

def formatGenomicVar(listGenomicData):
    outputListGenomicData = []
    # Genomic Variants file
    
    for genomicData in listGenomicData:
        dictGenomicData = {}
        if 'proteinChange' in genomicData:
            dictGenomicData['identifiers'] = {
                'proteinHGVSIds': [genomicData['proteinChange']],
                'genomicHGVSId': calculateHGVSId(genomicData['startPosition'], genomicData['endPosition'],
                                                 genomicData['chr'], genomicData['referenceAllele'],
                                                 genomicData['variantAllele'], genomicData['ncbiBuild'],
                                                 genomicData['variantType'])
                }
        if 'entrezGeneId' in genomicData:
            dictGenomicData['molecularAttributes'] = {'geneIds':[str(genomicData['entrezGeneId'])]}
        
        id = genomicData['patientId'] + '_' + genomicData['sampleId'] +\
            '_' + str(genomicData['startPosition']) + '_' + str(genomicData['endPosition']) +\
            '_' + genomicData['variantAllele'] + '_' + genomicData['referenceAllele']
        dictGenomicData['variantInternalId'] = id
        dictGenomicData['caseLevelData'] = [
            {
                'biosampleId': genomicData['sampleId'],
                'individualId': genomicData['patientId']
            }]

        # Write the variables align with the specs
        dictGenomicData['variation'] = {
                'alternateBases': genomicData['variantAllele'],
                'referenceBases': genomicData['referenceAllele'],
                'variantType': genomicData['variantType'],
                'location':{
                    'type':'SequenceLocation',
                    'sequence_id':'ga4gh:Not Available',
                    'interval': {
                        'type': 'SequenceInterval',
                        'end': {
                            'value':int(genomicData['startPosition']),
                            'type':'Number'
                        },
                        'start':{
                            'value': int(genomicData['endPosition']),
                            'type': 'Number'
                        }
                    }
                }
            }

        outputListGenomicData.append(dictGenomicData)
    return outputListGenomicData

def get_variants(entry_id: Optional[str], qparams: RequestParams):
    collection = 'g_variants'
    query = apply_request_parameters({}, qparams)
    LOG.debug("Query {}".format(query))

    # Search for the data in cBioPortal
    gdata_cBioPortal, lenData = query_API(query,
                                qparams.query.pagination.skip,
                                qparams.query.pagination.limit)
    # After gathering all the data, we convert it to the format in the specs
    docs = formatGenomicVar(gdata_cBioPortal)
    # Convert MongoDB Queries to cBioPortal API queries
    schema = DefaultSchemas.GENOMICVARIATIONS

    return schema, lenData, docs

def query_variant_per_id(variantId: str):
    variantSplitted = variantId.split('_')
    startPosition = variantSplitted[-4]
    endPosition = variantSplitted[-3]
    variantAllele = variantSplitted[-2]
    referenceAllele = variantSplitted[-1]
    ids = "".join(variantSplitted[:-4])
    queryToAPI = {'$and': [{'startPosition': ['==', int(startPosition)]},
                           {'endPosition': ['==', int(endPosition)]},
                           {'referenceAllele': ['==', referenceAllele]},
                           {'variantAllele': ['==', variantAllele]}]}
    gdata_cBioPortal, lenData = query_API(queryToAPI,
                                          0, 1000)
    for entry in gdata_cBioPortal:
        idsConcatenated = entry['patientId'] + entry['sampleId']
        if idsConcatenated != ids:
            continue
        return [entry], 1

# Need to do this
def get_variant_with_id(entry_id: Optional[str], qparams: RequestParams):

    schema = DefaultSchemas.GENOMICVARIATIONS

    gdata_cBioPortal, lenData = query_variant_per_id(entry_id)
    docs = formatGenomicVar(gdata_cBioPortal)
    # Convert MongoDB Queries to cBioPortal API queries
    schema = DefaultSchemas.GENOMICVARIATIONS

    return schema, lenData, docs

def get_biosamples_of_variant(entry_id: Optional[str], qparams: RequestParams):
    collection = 'g_variants'
    query = {"$and": [{"variantInternalId": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filter, collection)
    count = get_count(client.beacon.genomicVariations, query)
    biosample_ids = client.beacon.genomicVariations \
        .find_one(query, {"caseLevelData.biosampleId": 1, "_id": 0})
    
    biosample_ids=get_cross_query_variants(biosample_ids,'biosampleId','id')
    query = apply_filters(biosample_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.BIOSAMPLES
    count = get_count(client.beacon.biosamples, query)
    docs = get_documents(
        client.beacon.biosamples,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs


def get_individuals_of_variant(entry_id: Optional[str], qparams: RequestParams):
    collection = 'g_variants'
    query = {"$and": [{"variantInternalId": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    count = get_count(client.beacon.genomicVariations, query)
    individual_ids = client.beacon.genomicVariations \
        .find_one(query, {"caseLevelData.biosampleId": 1, "_id": 0})

    individual_ids = get_cross_query_variants(individual_ids,'biosampleId','id')
    query = apply_filters(individual_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.INDIVIDUALS
    count = get_count(client.beacon.individuals, query)
    docs = get_documents(
        client.beacon.individuals,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs


def get_runs_of_variant(entry_id: Optional[str], qparams: RequestParams):
    collection = 'g_variants'
    query = {"$and": [{"variantInternalId": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    count = get_count(client.beacon.genomicVariations, query)
    run_ids = client.beacon.genomicVariations \
        .find_one(query, {"caseLevelData.biosampleId": 1, "_id": 0})
    
    run_ids=get_cross_query_variants(run_ids,'biosampleId','biosampleId')
    query = apply_filters(run_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.RUNS
    count = get_count(client.beacon.runs, query)
    docs = get_documents(
        client.beacon.runs,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs


def get_analyses_of_variant(entry_id: Optional[str], qparams: RequestParams):
    collection = 'g_variants'
    query = {"$and": [{"variantInternalId": entry_id}]}
    query = apply_request_parameters(query, qparams)
    query = apply_filters(query, qparams.query.filters, collection)
    count = get_count(client.beacon.genomicVariations, query)
    analysis_ids = client.beacon.genomicVariations \
        .find_one(query, {"caseLevelData.biosampleId": 1, "_id": 0})

    analysis_ids=get_cross_query_variants(analysis_ids,'biosampleId','biosampleId')
    query = apply_filters(analysis_ids, qparams.query.filters, collection)

    schema = DefaultSchemas.ANALYSES
    count = get_count(client.beacon.analyses, query)
    docs = get_documents(
        client.beacon.analyses,
        query,
        qparams.query.pagination.skip,
        qparams.query.pagination.limit
    )
    return schema, count, docs
