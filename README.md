# cBioPortal on top Beacon RI-API

This project implements a Beacon v2 API on top of a cBioPortal. It is derived from the [Beacon Reference Implementation API](https://github.com/costero-e/beacon2-ri-api). As the only standard data in cBioPortal is the Genomic Variants, only this type of queries work.

## Before Running

Input two environment variables in your terminal: one for the url of your cBioPortal API, and the other for the name of the study.

```
export cBioPortal_URL="localhost:5050/" # The URL https://www.cbioportal.org/api is written by default
export study_name="nameOfYourStudy" # The study of cBioPortal "acc_tcga" is written by default

```

Python environment for downloading the libraries:

```
cd beacon2-ri-api/
python3 -m venv .pycBioPortalenv
source .pycBioPortalenv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
cd ..

```

## Usage

Run the script:

```
cd beacon2-ri-api/

python -m beacon basicConfigFile.ini
```

The `basicConfigFile.ini` is a configuration file used to query the Individual and Sample data from cBioPortal. It maps the variables from the cBioPortal study and the model schema properties from Beacon. The variable from cBioPortal are from the IMPaCT-Data guidelines to create a cBioPortal study (!add here guidelines!).

### Example:

- Retrieve your queries by changing the request parameters of the URL:

```
# Base path for genomic variants
http://localhost:5050/api/g_variants

http://localhost:5050/api/g_variants/?start=53529192,53529194&end=53529195&assemblyId=GRCh37&skip=0&limit=10

# Base path for individual data
http://localhost:5050/api/individuals

http://localhost:5050/api/individuals/?filter=NCIT:C16576


# Base path for biosample data
http://localhost:5050/api/biosamples

http://localhost:5050/api/biosamples/?filter=EFO:1001961

```

For genomic variants model, you can query the following terms: assemblyId, referenceName, start, end, referenceBases, alternateBases, variantType, mateName, gene.

For the other models, you filter by the ontologies available.

- Metadata paths:

```
http://localhost:5050/api/info
http://localhost:5050/api/map
http://localhost:5050/api/configuration
```
The metadata info can be changed in [conf.py](beacon2-ri-api/beacon/conf.py)


# Licencia

* GNU AFFERO GENERAL PUBLIC LICENSE Version 3. Ver [`LICENSE.md`](LICENSE.md).
* [![License: GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-brightgreen)](https://www.gnu.org/licenses/gpl-3.0.en.html) 

# Agradecimientos

_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._
